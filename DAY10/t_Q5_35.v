module t_Q5_35();

	reg x,y,rst,clk;
	wire z;
	
	Q5_35 dut(.x(x), .y(y), .clk(clk), .rst(rst), .z(z));
	
	integer i;
	initial
		begin
		clk = 1'b0;
		forever #5 clk = ~clk;
		end
	
	task initialize();
		begin
		{x,y,rst} = 3'b0;
		end
	endtask
	
	task inputs();
		begin
		for(i =0; i< 4; i = i+1)
			begin
			{x,y} = i;
			#10;
			end
		end
	endtask
	
	initial
		begin
		initialize;
		#5;
		rst = 1'b1;
		#5;
		rst = 1'b0;
		#5;
		x = 0; y = 0;
		#10;
		x = 0; y = 1;
		#10;
		x = 1; y = 0;
		#10;
		x = 1; y = 1;
		#10;
		x = 1; y = 0;
		#10;
		x = 1; y = 0;
		#10;
		x = 1; y = 1;
		#10;
		x = 0; y = 0;
				
		end
	
	initial #500 $finish;

endmodule