onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_Q5_35/clk
add wave -noupdate /t_Q5_35/rst
add wave -noupdate /t_Q5_35/x
add wave -noupdate /t_Q5_35/y
add wave -noupdate /t_Q5_35/dut/z
add wave -noupdate /t_Q5_35/dut/PS
add wave -noupdate /t_Q5_35/dut/NS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {25 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {126 ps}
