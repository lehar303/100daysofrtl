module Q5_35(
	input x,y,clk,rst,
	output z);
	
	parameter S0 = 2'b00;
	parameter S1 = 2'b01;
	parameter S2 = 2'b10;
	parameter S3 = 2'b11;
	
	reg [1:0]PS,NS;
	
	always@(posedge clk)
		if(rst)
			PS <= S0;
		else
			PS <= NS;
			
	always@(PS,x,y)
		begin
		case(PS)
			S0: case({x,y})
					2'b00: NS = 2'b00;
					2'b01: NS = 2'b00;
					2'b10: NS = 2'b01;
					2'b11: NS = 2'b11;
				endcase
				
			S1: case({x,y})
					2'b00: NS = 2'b00;
					2'b01: NS = 2'b00;
					2'b10: NS = 2'b10;
					2'b11: NS = 2'b10;
				endcase
				
			S2: case({x,y})
					2'b00: NS = 2'b00;
					2'b01: NS = 2'b00;
					2'b10: NS = 2'b11;
					2'b11: NS = 2'b01;
				endcase
				
			S3: case({x,y})
					2'b00: NS = 2'b00;
					2'b01: NS = 2'b00;
					2'b10: NS = 2'b11;
					2'b11: NS = 2'b11;
				endcase	
		endcase
		end
		
	assign z = ((PS == S2) || (PS == S3));
	
	
endmodule