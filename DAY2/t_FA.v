module t_FA();

	reg a,b,cin;
	wire s,c;
	integer i;

	FA dut(a,b,cin,s,c);

	initial
		begin
		$monitor("| A | B | C | Sum | Carry |");
		a=1'b0;
		b=1'b0;
		cin=1'b0;
		end

	initial
		begin
		for(i=0;i<8;i=i+1)
			begin
			{a,b,cin} = i;
			#10;
			end
		end

	initial
		begin
		#5;
		$monitor("| %b | %b | %b |  %b  |   %b   |",a,b,cin,s,c);
		end

	initial 
		#200 $finish;

endmodule