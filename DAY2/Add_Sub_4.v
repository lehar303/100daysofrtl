module Add_Sub_4(
	input [3:0]A, [3:0]B,
	input M,
	output [3:0]O,
	output Cout
	);
	
	wire [3:0]X;
	wire [4:1]Carry;
	
	xor
	G0(X[0],M,B[0]),
	G1(X[1],M,B[1]),
	G2(X[2],M,B[2]),
	G3(X[3],M,B[3]);
	
	FA fa0(A[0],X[0],M,O[0],Carry[1]);
	FA fa1(A[1],X[1],Carry[1],O[1],Carry[2]);
	FA fa2(A[2],X[2],Carry[2],O[2],Carry[3]);
	FA fa3(A[3],X[3],Carry[3],O[3],Carry[4]);
	
	assign Cout = Carry[4];
	
	
endmodule
	