module t_Add_Sub_4();

	reg [3:0]A,B;
	reg C;
	wire [3:0]O;
	wire Cout;
	integer i,j;
	
	Add_Sub_4 dut(A,B,C,O,Cout);
	
	initial #11000 $finish;
	
	task initialize;
		begin
		A = 4'b0;
		B = 4'b0;
		C = 0;
		end
	endtask
	
	task add(input [3:0]a,[3:0]b);
		begin
		A = a;
		B = b;
		C = 0;
		end
	endtask
	
	task sub(input [3:0]a,[3:0]b);
		begin
		A = a;
		B = b;
		C = 1;
		end
	endtask
	
	
	initial
		begin
		initialize;
		for(i=0;i<16;i = i+1)
			begin
			for(j = 0;j<16; j=j+1)
				begin
				add(i,j);
				#20;
				sub(i,j);
				#20;
				end
			end
		end
		
endmodule