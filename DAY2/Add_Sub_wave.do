onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /t_Add_Sub_4/A
add wave -noupdate -radix unsigned /t_Add_Sub_4/B
add wave -noupdate /t_Add_Sub_4/C
add wave -noupdate -radix unsigned /t_Add_Sub_4/O
add wave -noupdate /t_Add_Sub_4/Cout
add wave -noupdate /t_Add_Sub_4/i
add wave -noupdate /t_Add_Sub_4/j
add wave -noupdate /t_Add_Sub_4/dut/A
add wave -noupdate /t_Add_Sub_4/dut/B
add wave -noupdate /t_Add_Sub_4/dut/M
add wave -noupdate /t_Add_Sub_4/dut/O
add wave -noupdate /t_Add_Sub_4/dut/Cout
add wave -noupdate /t_Add_Sub_4/dut/X
add wave -noupdate /t_Add_Sub_4/dut/Carry
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9960 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {250 ps}
