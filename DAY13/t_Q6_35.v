module t_Q6_31();

	reg [3:0]I;
	reg R_n,clk;
	wire [3:0]A;
	
	//Q6_31_behav dut(.I(I), .R_n(R_n), .clk(clk), .A(A));
	Q6_31_struct dut(.I(I), .R_n(R_n), .clk(clk), .A(A));
	
	initial
		begin
		clk = 1'b0;
		forever #5 clk = ~clk;
		end 
		
	initial
		begin
		R_n = 1'b0;
		I = 4'b1;
		#10;
		R_n = 1'b1;
		#10;
		I = 4'b1010;
		#10;
		I = 4'b0101;
		end
	
	initial #100 $finish;

endmodule