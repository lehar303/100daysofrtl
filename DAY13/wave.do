onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_Q6_31/clk
add wave -noupdate /t_Q6_31/I
add wave -noupdate /t_Q6_31/R_n
add wave -noupdate /t_Q6_31/A
add wave -noupdate /t_Q6_31/dut/I
add wave -noupdate /t_Q6_31/dut/R_n
add wave -noupdate /t_Q6_31/dut/clk
add wave -noupdate /t_Q6_31/dut/A
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {128 ps}
