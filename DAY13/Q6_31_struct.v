module Q6_31_struct(
	input [3:0]I,
	input R_n,clk,
	output [3:0]A);
	
	DFF D0(.in(I[0]), .clk(clk), .rst_n(R_n), .out(A[0]));
	DFF D1(.in(I[1]), .clk(clk), .rst_n(R_n), .out(A[1]));
	DFF D2(.in(I[2]), .clk(clk), .rst_n(R_n), .out(A[2]));
	DFF D3(.in(I[3]), .clk(clk), .rst_n(R_n), .out(A[3]));		
	
	
endmodule

module DFF(
	input in,clk,rst_n,
	output reg out);
	
	always@(posedge clk,negedge rst_n)
		if(!rst_n)
			out = 1'b0;
		else
			out = in;
			
endmodule