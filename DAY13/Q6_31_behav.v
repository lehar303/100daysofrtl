module Q6_31_behav(
	input [3:0]I,
	input R_n,clk,
	output reg [3:0]A);
	
	always@(posedge clk,negedge R_n)
		begin
		if(!R_n)
			A = 4'b0;
		else
			A = I;
		end
	
	
endmodule