module t_BCD_Ex3();

	reg A,B,C,D;
	wire [3:0]Y;
	integer i;
	
	//BCD_EX3_Gate dut(A,B,C,D,Y);
	//BCD_Ex3_Dataflow dut(A,B,C,D,Y);
	BCD_Ex3_Behav dut(A,B,C,D,Y);
	
	task initialize();
		{A,B,C,D} = 4'b0;
	endtask
	
	task inputs();
		begin
		for(i = 0;i < 10; i=i+1)
			begin
			{A,B,C,D} = i;
			#10;
			end
		end
	endtask
	
	initial
		begin
		initialize;
		#5;
		inputs;
		end
		
	initial #2000 $finish;

endmodule