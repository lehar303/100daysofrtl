onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_BCD_Ex3/A
add wave -noupdate /t_BCD_Ex3/B
add wave -noupdate /t_BCD_Ex3/C
add wave -noupdate /t_BCD_Ex3/D
add wave -noupdate /t_BCD_Ex3/Y
add wave -noupdate /t_BCD_Ex3/i
add wave -noupdate /t_BCD_Ex3/dut/A
add wave -noupdate /t_BCD_Ex3/dut/B
add wave -noupdate /t_BCD_Ex3/dut/C
add wave -noupdate /t_BCD_Ex3/dut/D
add wave -noupdate /t_BCD_Ex3/dut/Y
add wave -noupdate /t_BCD_Ex3/dut/BD_out
add wave -noupdate /t_BCD_Ex3/dut/BC_out
add wave -noupdate /t_BCD_Ex3/dut/BbarC_out
add wave -noupdate /t_BCD_Ex3/dut/BbarD_out
add wave -noupdate /t_BCD_Ex3/dut/BCbarDBar_out
add wave -noupdate /t_BCD_Ex3/dut/CD_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {126 ps}
