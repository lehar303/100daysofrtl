module BCD_Ex3_Behav(
	input A,B,C,D,
	output reg [3:0]Y);
	
	integer i;
	always@(A,B,C,D)
		begin
		Y = {A,B,C,D} + 4'b0011;
		end	
	
endmodule