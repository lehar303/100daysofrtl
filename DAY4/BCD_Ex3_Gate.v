module BCD_EX3_Gate(
	input A,B,C,D,
	output [3:0]Y);
	
	wire BD_out,BC_out,BbarC_out,BbarD_out,BCbarDBar_out,CD_out;
	and
	BD(BD_out,B,D),
	BC(BC_out,B,C),
	B_barC(BbarC_out,~B,C),
	B_barD(BbarD_out,~B,D),
	BC_barD_bar(BCbarDBar_out,B,~C,~D);
	
	xnor
	CD(Y[1],C,D);
	
	or
	Y3(Y[3],A,BC_out,BD_out),
	Y2(Y[2],BbarC_out,BbarD_out,BCbarDBar_out);
	
	not
	D_bar(Y[0],D);
	
	
endmodule