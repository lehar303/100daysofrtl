module BCD_Ex3_Dataflow(
	input A,B,C,D,
	output [3:0]Y);
	
	assign Y[3] = A | (B&D) | (B&C);
	assign Y[2] = (~B&C) | (~B&D) | (B&~C&~D);
	assign Y[1] = C ~^ D;
	assign Y[0] = ~D;
	
	
endmodule