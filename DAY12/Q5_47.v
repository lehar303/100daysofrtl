module Q5_47(
	input Run,rst,clk,
	output out);
	
	parameter S0  = 4'b0000;
	parameter S1  = 4'b0010;
	parameter S2  = 4'b0100;
	parameter S3  = 4'b0110;
	parameter S4  = 4'b1000;
	parameter S5  = 4'b1010;
	parameter S6  = 4'b1100;
	parameter S7  = 4'b1110;
	
	reg [3:0]PS,NS;
	
	always@(posedge clk)
		begin
		if(rst)
			PS = S0;
		else
			PS = NS;
		end
		
	always@(Run,PS)
		begin
		case(PS)
			S0: if(Run)
					NS = S1;
				else
					NS = S0;
			S1: if(Run)
					NS  = S2;
				else
					NS = S1;
			S2: if(Run)
					NS = S3;
				else
					NS = S2;
			S3: if(Run)
					NS = S4;
				else
					NS = S3;
			S4: if(Run)
					NS = S5;
				else
					NS = S4;
			S5: if(Run)
					NS = S6;
				else
					NS = S5;
			S6: if(Run)
					NS = S7;
				else
					NS = S6;
			S7: if(Run)
					NS = S0;
				else
					NS = S7;
					
		endcase
		end
		
	assign out = (Run == 1)?1:0;
	
	

endmodule