module t_Q5_47();

	reg Run,clk,rst;
	wire out;
	
	Q5_47 dut(.Run(Run), .clk(clk), .rst(rst), .out(out));
	
	initial
		begin
		clk = 1'b0;
		forever #5 clk = ~clk;
		end
		
	initial
		begin
		rst = 1'b1;
		Run = 1'b0;
		#10;
		rst = 1'b0;
		Run = 1'b1;
		#50;
		Run = 1'b0;
		#30;
		Run = 1'b1;
		end
		
	initial #200 $finish;
		


endmodule