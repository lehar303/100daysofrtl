module t_Q6_38();

	reg [3:0]in;
	reg rst,clk;
	reg [1:0] sel;
	wire [3:0]count;
	
	Q6_38 dut(.in(in), .sel(sel), .clk(clk), .rst(rst), .count(count));
	
	initial
		begin
		clk = 1'b0;
		forever
			#5 clk =~clk;
		end 
			
	initial
		begin
		rst = 1'b1;
		in = 4'b0;
		sel = 2'b0;
		#7;
		rst = 1'b0;
		#7;
		sel = 2'b01;
		#20;
		sel = 2'b10;
		#20;
		sel = 2'b00;
		in = 4'b1000;
		#10;
		sel = 2'b10;
		end
		
	initial #100 $finish;
	

endmodule