module Q6_38(
	input [3:0]in,
	input [1:0] sel,
	input clk,rst,
	output reg [3:0]count);
	
	always@(posedge clk)
		begin
		if(rst)
			count <= 4'b0;
		else if(sel == 2'b00)
			count <= in;
		else if(sel == 2'b01)
			count <= count + 1;
		else if(sel == 2'b10)
			count <= count - 1;
		else
			count <= count;
		end 


endmodule