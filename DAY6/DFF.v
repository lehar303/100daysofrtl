module DFF(
	input d,clr,preset,clk,
	output reg q,qbar);
	
	always@(posedge clk or posedge clr or posedge preset)
		begin
		if(clr)
			q <= 1'b0;
		else if(preset)
			q <= 1'b1;
		else	
			q<=d;
		end
		
	always@(q)
		qbar <= ~q;
	
	
endmodule