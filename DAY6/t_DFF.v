module t_DFF();
	reg d,clr,preset,clk;
	wire q,qbar;
	
	DFF dut(.d(d),.clr(clr),.preset(preset),.clk(clk),.q(q),.qbar(qbar));
	
	integer i;
	initial
		begin
		clk = 1'b0;
		forever
			#5 clk = ~clk;
		end
		
	task initialize();
		{d,clr,preset} = 3'b000;
	endtask

	task inputs();
		begin
		for(i = 0;i<8;i = i+1)
			begin
			{clr,preset,d} = i;
			#5;
			end
		end
	endtask
	
	initial
		begin
		initialize;
		inputs;
		end
	
	initial #200 $finish;
	
	
endmodule