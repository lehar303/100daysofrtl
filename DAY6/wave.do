onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_DFF/clk
add wave -noupdate /t_DFF/d
add wave -noupdate /t_DFF/preset
add wave -noupdate /t_DFF/clr
add wave -noupdate /t_DFF/q
add wave -noupdate /t_DFF/qbar
add wave -noupdate /t_DFF/i
add wave -noupdate /t_DFF/dut/d
add wave -noupdate /t_DFF/dut/clr
add wave -noupdate /t_DFF/dut/preset
add wave -noupdate /t_DFF/dut/clk
add wave -noupdate /t_DFF/dut/q
add wave -noupdate /t_DFF/dut/qbar
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {12 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {64 ps}
