module quad_mux_2_1(
	input [3:0]A,[3:0]B,
	input S,E,
	output [3:0]Y);
	
	wire S_bar,E_bar,G3_out,G4_out,G5_out,G6_out,G7_out,G8_out,G9_out,G10_out;
	not
	G1(S_bar,S),
	G2(E_bar,E);
	
	and
	G3(G3_out,A[0],S_bar,E_bar),
	G4(G4_out,A[1],S_bar,E_bar),
	G5(G5_out,A[2],S_bar,E_bar),
	G6(G6_out,A[3],S_bar,E_bar),
	G7(G7_out,B[0],S,E_bar),
	G8(G8_out,B[1],S,E_bar),
	G9(G9_out,B[2],S,E_bar),
	G10(G10_out,B[3],S,E_bar);
	
	or
	G11(Y[0],G3_out,G7_out),
	G12(Y[1],G4_out,G8_out),
	G13(Y[2],G5_out,G9_out),
	G14(Y[3],G6_out,G10_out);
	
	
endmodule