module t_quad_mux_2_1();

	reg [3:0]A,B;
	reg S,E;
	wire [3:0]Y;
	integer j;
	
	quad_mux_2_1 dut(A,B,S,E,Y);
	
	initial #20000 $finish;
	
	task initialize;
		begin
		A = 4'b0;
		B = 4'b0;
		S = 1'b0;
		E = 1'b1;
		end
	endtask
	
	task ctrl_inputs(input select,input enable);
		begin
		S = select;
		E = enable;
		end
	endtask
	
	task data_inputs();
		begin
		for(j=0;j<256;j = j+1)
			begin
			{A,B} = j;
			#15;
			end
		end
	endtask
	
	initial
		begin
		initialize;
		#5;
		ctrl_inputs(0,0);
		#10;
		data_inputs;
		#10;
		ctrl_inputs(0,1);
		#10;
		data_inputs;
		#10;
		ctrl_inputs(1,0);
		#10;
		data_inputs;
		#10;
		ctrl_inputs(1,1);
		#10;
		data_inputs;
		
		end
	
	

endmodule