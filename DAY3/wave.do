onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_quad_mux_2_1/A
add wave -noupdate /t_quad_mux_2_1/B
add wave -noupdate /t_quad_mux_2_1/S
add wave -noupdate /t_quad_mux_2_1/E
add wave -noupdate /t_quad_mux_2_1/Y
add wave -noupdate /t_quad_mux_2_1/j
add wave -noupdate /t_quad_mux_2_1/dut/A
add wave -noupdate /t_quad_mux_2_1/dut/B
add wave -noupdate /t_quad_mux_2_1/dut/S
add wave -noupdate /t_quad_mux_2_1/dut/E
add wave -noupdate /t_quad_mux_2_1/dut/Y
add wave -noupdate /t_quad_mux_2_1/dut/S_bar
add wave -noupdate /t_quad_mux_2_1/dut/E_bar
add wave -noupdate /t_quad_mux_2_1/dut/G3_out
add wave -noupdate /t_quad_mux_2_1/dut/G4_out
add wave -noupdate /t_quad_mux_2_1/dut/G5_out
add wave -noupdate /t_quad_mux_2_1/dut/G6_out
add wave -noupdate /t_quad_mux_2_1/dut/G7_out
add wave -noupdate /t_quad_mux_2_1/dut/G8_out
add wave -noupdate /t_quad_mux_2_1/dut/G9_out
add wave -noupdate /t_quad_mux_2_1/dut/G10_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15430 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {15335 ps} {15461 ps}
