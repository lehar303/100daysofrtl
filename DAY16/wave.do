onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_edge_detector/in
add wave -noupdate /t_edge_detector/pe
add wave -noupdate /t_edge_detector/ne
add wave -noupdate /t_edge_detector/dut/in
add wave -noupdate /t_edge_detector/dut/pe
add wave -noupdate /t_edge_detector/dut/ne
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {29 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {14 ps} {78 ps}
