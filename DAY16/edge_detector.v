module edge_detector(
	input in,
	output reg pe,ne);
	
	always@(posedge in)
		begin
		pe = in;
		#5 pe = ~pe;
		end
		
	always@(negedge in)
		begin
		ne = ~in;
		#5 ne = ~ne;
		end
		
endmodule