module t_edge_detector();

	reg in;
	wire pe,ne;
	
	edge_detector dut(.in(in), .pe(pe), .ne(ne));
	
		
	initial
		begin
		in = 1'b0;
		#5 in = 1'b1;
		#20 in = 1'b0;
		#7 in = 1'b1;
		#3 in = 1'b0;
		#9 in = 1'b1;
		#6 in = 1'b0;
		#2 in = 1'b1;
		#8 in = 1'b0;
		#1 in = 1'b1;
		#8 in = 1'b0;
		#40 in = 1'b1;
		#30 in = 1'b0;
		#1 in = 1'b1;
		end 
		
	initial #100 $finish;

endmodule