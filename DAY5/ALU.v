module ALU(
	input [2:0]sel,
	input [15:0]A,[15:0]B,
	input en,
	output reg [15:0]y);
	
	always@(A,B,sel,en)
		begin
		if(en)
			begin
			case(sel)
				3'b000: y = 16'b0;
				3'b001: y = A & B;
				3'b010: y = A | B;
				3'b011: y = A ^ B;
				3'b100: y = ~A;
				3'b101: y = A - B;
				3'b110: y = A + B;
				3'b111: y = 16'hFFFF;
				default: y = 16'bX;
			endcase
			end
		else
			y = 16'bX;
		end
	
endmodule