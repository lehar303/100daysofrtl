module t_ALU();
	reg [2:0]sel;
	reg [15:0]A,B;
	reg en;
	wire [15:0]Y;
	
	ALU dut (sel,A,B,en,Y);
	initial #200 $finish;
	
	initial fork
	#5 en = 1;
	#5 begin A = 8'hAA; B = 8'h55; end // Expect y = 8'd0
	#10 begin sel = 3'b000; A = 8'hAA; B = 8'h55; end // y = 8'b000 Expect y = 8'd0
	#20 begin sel = 3'b001; A = 8'hAA; B = 8'hAA; end // y = A & B Expect y = 8'hAA = 8'1010_1010
	#30 begin sel = 3'b001; A = 8'h55; B = 8'h55; end // y = A & B Expect y = 8'h55 = 8'b0101_0101
	#40 begin sel = 3'b010; A = 8'h55; B = 8'h55; end // y = A | B Expect y = 8'h55 = 8'b0101_0101
	#50 begin sel = 3'b010; A = 8'hAA; B = 8'hAA; end // y = A | BExpect y = 8'hAA = 8'b1010_1010
	#60 begin sel = 3'b011; A = 8'h55; B = 8'h55; end // y = A ^ B Expect y = 8'd0
	#70 begin sel = 3'b011; A = 8'hAA; B = 8'h55; end // y = A ^ B Expect y = 8'hFF = 8'b1111_1111
	#80 begin sel = 3'b100; A = 8'h55; B = 8'h00; end // y = A + B Expect y = 8'h55 = 8'b0101_0101
	#90 begin sel = 3'b100; A = 8'hAA; B = 8'h55; end // y = A + B Expect y = 8'hFF = 8'b1111_1111
	#100 en = 0;
	#115 en = 1;
	#110 begin sel = 3'b101; A = 8'hAA; B = 8'h55; end // y = A – B Expect y = 8'h55 = 8'b0101_0101
	#120 begin sel = 3'b101; A = 8'h55; B = 8'hAA; end // y = A – B Expect y = 8'hab = 8'b1010_1011
	#130 begin sel = 3'b110; A = 8'hFF; end // y = ~A Expect y = 8'd0
	#140 begin sel = 3'b110; A = 8'd0; end // y = ~A Expect y = 8'hFF = 8'b1111_1111
	#150 begin sel = 3'b110; A = 8'hFF; end // y = ~A Expect y = 8'd0
	#160 begin sel = 3'b111; end // y = 8'hFF Expect y = 8'hFF = 8'b1111_1111
	join
	
	
endmodule
	