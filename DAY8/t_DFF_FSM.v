module t_DFF_FSM();

	reg A,B,clk,rst;
	wire Y;
	
	DFF_FSM dut(.x(A),.y(B),.clk(clk),.rst(rst),.a(Y));
	
	initial
		begin
		clk = 1'b0;
		forever
			#5 clk = ~clk;
		end
		
	task initialize();
		begin
		{A,B} = 2'b0;
		rst = 3'b1;
		end
	endtask
	
	initial
		begin
		initialize;
		#10;
		rst = 1'b0;
		#10;
		rst = 1'b1;
		#10;
		rst = 1'b0;
		#10;
		A = 0;
		B = 0;
		#10;
		A = 0;
		B = 1;
		#10;
		A = 1;
		B = 0;
		#10;
		A = 1;
		B = 1;
				
		end
		
	initial #200 $finish;
	
endmodule