module DFF_FSM(
	input x,y,clk,rst,
	output a);
	
	parameter S0 = 1'b0;
	parameter S1 = 1'b1;
	
	reg PS,NS;
	
	always@(posedge clk)
		if(rst)
			PS <= S0;
		else
			PS <= NS;
		
	always@(PS,x,y)
		begin
		case(PS)
			S0: case({x,y})
				2'b00: NS = S0;
				2'b01: NS = S1;
				2'b10: NS = S1;
				2'b11: NS = S0;
				endcase
			S1: case({x,y})
				2'b00: NS = S1;
				2'b01: NS = S0;
				2'b10: NS = S0;
				2'b11: NS = S1;
				endcase
		endcase
		end
		
	assign a = PS;
	
endmodule