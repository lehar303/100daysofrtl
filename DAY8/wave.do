onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_DFF_FSM/clk
add wave -noupdate /t_DFF_FSM/A
add wave -noupdate /t_DFF_FSM/B
add wave -noupdate /t_DFF_FSM/rst
add wave -noupdate /t_DFF_FSM/Y
add wave -noupdate /t_DFF_FSM/dut/x
add wave -noupdate /t_DFF_FSM/dut/y
add wave -noupdate /t_DFF_FSM/dut/clk
add wave -noupdate /t_DFF_FSM/dut/rst
add wave -noupdate /t_DFF_FSM/dut/a
add wave -noupdate /t_DFF_FSM/dut/PS
add wave -noupdate /t_DFF_FSM/dut/NS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {77 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {126 ps}
