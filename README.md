# 100daysofRTL

## Description
Taking up 100 days of RTL challenege. Will solve an RTL Design problem everyday, for 100 days. Most problems are taken from **Digital Design with an Introduction to Verilog**, 5th Edition by Morris Mano and Micheal D. Ciletti. Others are sourced online.


> DAY1: 4 input Priority Encode, problem 4.36

> DAY2: 4 bit Adder Subtractor, problem 4.37

> DAY3: Quadraple 2x1 Mux, problem 4.38

> DAY4: BCD to Excess 3 convertor, problem 4.42

> DAY5: ALU, prroblem 4.44 and 4.48 

> DAY6: Async preset and clear DFF, problem 5.24 

> DAY7: JK Flipflop, problem 5.26 

> DAY8: DFF with FSM, problem 5.28 a)

> DAY9: waveforms, problem 5.32 

> DAY10: problem 5.35 

> DAY11: problem 5.46 

> DAY12: problem 5.47 

> DAY13: problem 6.31 

> DAY14: problem 6.32 a)

> DAY15: problem 6.38 

> DAY16: Edge detector

> DAY17: Priority Arbiter

> DAY18: Round Robin Arbiter