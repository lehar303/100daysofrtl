onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_Parallel_Reg_4/I
add wave -noupdate /t_Parallel_Reg_4/load
add wave -noupdate /t_Parallel_Reg_4/clk
add wave -noupdate /t_Parallel_Reg_4/clr
add wave -noupdate /t_Parallel_Reg_4/A
add wave -noupdate /t_Parallel_Reg_4/dut/I
add wave -noupdate /t_Parallel_Reg_4/dut/load
add wave -noupdate /t_Parallel_Reg_4/dut/clear
add wave -noupdate /t_Parallel_Reg_4/dut/clk
add wave -noupdate /t_Parallel_Reg_4/dut/Y
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {40 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 187
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {120 ps}
