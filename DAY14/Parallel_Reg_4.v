module Parallel_Reg_4(
	input [3:0]I,
	input load,clear,clk,
	output reg [3:0]Y);
	
	always@(posedge clk, clear)
		begin
		if(clear)
			Y = 4'b0;
		else if(load)
			Y = I;
		end 
	
	
endmodule