module t_Parallel_Reg_4();

	reg [3:0]I;
	reg load,clk,clr;
	wire [3:0]A;
	
	Parallel_Reg_4 dut(.I(I), .clear(clr), .clk(clk), .load(load), .Y(A));
	
	initial
		begin
		clk = 1'b0;
		forever #5 clk = ~clk;
		end 
		
	initial
		begin
		clr = 1'b1;
		I = 4'b1;
		#10;
		clr = 1'b0;
		load = 1'b1;
		#10;
		I = 4'b1010;
		#7;
		load = 1'b0;
		#10;
		I = 4'b0101;
		#12;
		load = 1'b1;
		end
	
	initial #100 $finish;

endmodule