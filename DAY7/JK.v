module JK(
	input j,k,clk,
	output reg q,qbar);
	
	always@(posedge clk)
		begin
		if(!j && !k)
			q <= q;
		else if(!j && k)
			q <= 1'b0;
		else if (j && !k)
			q <= 1'b1;
		else
			q <= ~q;
		end
		
	always@(q)
		qbar <= ~q;
	
endmodule