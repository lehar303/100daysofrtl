module t_JK();

	reg j,k,clk;
	wire q,qbar;
	
	integer i;
	
	JK dut(.j(j),.k(k),.clk(clk),.q(q),.qbar(qbar));
	
	initial
		begin
		clk = 1'b0;
			forever
				#10 clk = ~clk;
		end
		
	task init();
		begin
		{j,k} = 2'b0;
		end
	endtask
	
	task inputs();
		begin
		for(i = 0; i<4; i=i+1)
			begin
			{j,k} = i;
			#15;
			end
		end
	endtask
	
	initial
		begin
		init;
		#10;
		inputs;
		end
	
	initial #200 $finish;

endmodule