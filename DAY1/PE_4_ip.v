module PE_4_ip(
	input [3:0]D,
	output x,y,v);

wire D2_bar,G3_op,G5_op;	
	or
	G1(x,D[2],D[3]),
	G4(y,D[3],G3_op),
	G5(G5_op,D[0],D[1]),
	G6(v,G5_op,x);
	
	not
	G2(D2_bar,D[2]);
	
	and
	G3(G3_op,D[1],D2_bar);
	
	
endmodule