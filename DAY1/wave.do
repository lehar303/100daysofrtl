onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand /t_PE_4_ip/DUT1/D
add wave -noupdate /t_PE_4_ip/DUT1/x
add wave -noupdate /t_PE_4_ip/DUT1/y
add wave -noupdate /t_PE_4_ip/DUT1/v
add wave -noupdate /t_PE_4_ip/DUT1/D2_bar
add wave -noupdate /t_PE_4_ip/DUT1/G3_op
add wave -noupdate /t_PE_4_ip/DUT1/G5_op
add wave -noupdate /t_PE_4_ip/D
add wave -noupdate /t_PE_4_ip/x
add wave -noupdate /t_PE_4_ip/y
add wave -noupdate /t_PE_4_ip/v
add wave -noupdate /t_PE_4_ip/i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {64 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {205 ps}
