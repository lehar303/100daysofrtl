module t_PE_4_ip();

	reg [3:0]D;
	wire x,y,v;
	integer i;
	
	PE_4_ip DUT1(D,x,y,v);
	
	initial #200 $finish;
	
	initial
		begin
			for(i = 0;i<16;i=i+1)
				begin
				D = i;
				#10;
				end
		end
endmodule