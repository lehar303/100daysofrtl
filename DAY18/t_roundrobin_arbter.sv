module t_roundrobin_arbiter();

	parameter NUM_REQ = 4;
	
	reg [NUM_REQ - 1:0] request;
	reg clk,rstN;
	wire [NUM_REQ - 1:0] access;
	
	integer i;
	
	RoundRobinArbiter#(NUM_REQ) dut (.req(request), .grant(access), .clk(clk), .rstN(rstN));
	
	initial 
	   begin
		clk = 1'b0;
		forever #5 clk = ~ clk;
		end
	
	initial
		begin
		rstN = 1'b0;
		#10;
		rstN = 1'b1;
		for(i = 0; i<2^NUM_REQ; i=i+1)
			begin
			request = i;
			#5;
			end
		end

endmodule