module Q5_46(
	input rst,clk,x_in,
	output y_out);
	
	parameter S0 = 3'b000;
	parameter S1 = 3'b001;
	parameter S2 = 3'b010;
	parameter S3 = 3'b011;
	parameter S4 = 3'b100;
	parameter S5 = 3'b101;
	
	reg [2:0]PS,NS;
	
	always@(posedge clk)
		begin
		if(!rst)
			PS = S0;
		else
			PS = NS;
		end
		
	always@(PS,x_in)
		begin
		case(PS)
			S0: begin
					if(x_in)
						NS = S1;
					else
						NS = S0;
					end
			S1: NS = S2;
			S2: NS = S3;
			S3: NS = S4;
			S4: NS = S5;
			S5: NS = S0;
			default: NS = S0;
		endcase
		end
		
	assign y_out = ((PS == S0) || (PS == S1) || (PS == S2));	
	
endmodule