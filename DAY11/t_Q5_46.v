module t_Q5_46();

	reg clk,rst,x_in;
	wire y_out;
	
	Q5_46 dut(.clk(clk), .rst(rst), .x_in(x_in), .y_out(y_out));
	
	initial
		begin
		clk = 1'b0;
		forever
			#5 clk = ~clk;
		end
	
	initial
		begin
		rst = 1'b0;
		#10 rst = 1'b1;
		#10 x_in = 0;
		#10 x_in = 1;
		#5 x_in = 1;
		#5 x_in = 0;
		end
	
	initial #100 $finish;

endmodule