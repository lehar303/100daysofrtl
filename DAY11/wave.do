onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_Q5_46/clk
add wave -noupdate /t_Q5_46/rst
add wave -noupdate /t_Q5_46/x_in
add wave -noupdate /t_Q5_46/y_out
add wave -noupdate /t_Q5_46/dut/rst
add wave -noupdate /t_Q5_46/dut/clk
add wave -noupdate /t_Q5_46/dut/x_in
add wave -noupdate /t_Q5_46/dut/y_out
add wave -noupdate /t_Q5_46/dut/PS
add wave -noupdate /t_Q5_46/dut/NS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {64 ps}
