onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /t_priority_arbiter/request
add wave -noupdate /t_priority_arbiter/access
add wave -noupdate /t_priority_arbiter/i
add wave -noupdate /t_priority_arbiter/dut/request
add wave -noupdate /t_priority_arbiter/dut/access
add wave -noupdate /t_priority_arbiter/dut/i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 189
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {30 ps}
