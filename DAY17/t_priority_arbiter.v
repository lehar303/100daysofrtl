module t_priority_arbiter();

	parameter NUM_REQ = 4;
	
	reg [NUM_REQ - 1:0] request;
	wire [NUM_REQ - 1:0] access;
	
	integer i;
	
	priority_arbiter#(NUM_REQ) dut (.request(request), .access(access));
	
	initial
		begin
		for(i = 0; i<2^NUM_REQ; i=i+1)
			begin
			request = i;
			#5;
			end
		end

endmodule
