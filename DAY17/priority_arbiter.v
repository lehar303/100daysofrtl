module priority_arbiter #(parameter NUM_REQ = 4)
(
	input [NUM_REQ - 1 :0] request,
	output reg [NUM_REQ - 1 :0] access);
	
	integer i;
	
		initial
			access = 0;
			
		always@(request)
			begin : grant_access
			access = 0;
			for(i = 0; i< NUM_REQ; i = i+1)
				begin
				if(request[i])
					begin
					access[i] = 1'b1;
					disable grant_access;
					end 
				end
			end 
			
endmodule