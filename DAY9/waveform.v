module waveform();

	reg A,B,C,D,E,F,en;
	
	initial
		begin
		en = 1'b0; A = 1'b1; B = 1'b0; C = 1'b0; D = 1'b0; E = 1'b1; F = 1'b1;
		#10;
		A = 1'b0; B = 1'b1; C = 1'b1;
		#10;
		A = 1'b1; B = 1'b0; D = 1'b1; E = 1'b0;
		#10;
		B = 1'b1; E = 1'b1; F = 1'b0;
		#10;
		en = 1'b1;B = 1'b0; D = 1'b0; F = 1'b1;
		#10;
		B = 1'b1; 
		#10;
		B = 1'b0; D = 1'b1;
		#10;
		B = 1'b1;
		
		
		end

endmodule