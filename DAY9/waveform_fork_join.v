module waveform_fork_join();

	reg A,B,C,D,E,F,en;
	
	initial
		fork
			en = 1'b0; A = 1'b1; B = 1'b0; C = 1'b0; D = 1'b0; E = 1'b1; F = 1'b1;
			#10	begin
			A = 1'b0; B = 1'b1; C = 1'b1; 
			end
			#20 begin
			A = 1'b1; B = 1'b0; D = 1'b1; E = 1'b0; 
			end
			#30 begin
			B = 1'b1; E = 1'b1; F = 1'b0; 
			end
			#40 begin
			en = 1'b1; B = 1'b0; D = 1'b0; F = 1'b1;
			end
			#50 begin
			B = 1'b1; 
			end
			#60 begin
			B = 1'b0; D = 1'b1; 
			end
			#70	B = 1;
			#80 $finish;
		join

endmodule